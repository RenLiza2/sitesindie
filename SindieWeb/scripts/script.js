//переходы между элементами на одной странице
document.onclick = function (event) {
   var target = event.target;
   var id = target.getAttribute('data-list');
   if (!id) return;
   var elem = document.getElementById(id);
   elem.hidden = !elem.hidden;
}

//постенькая проверка формы
function validateComments(input) {
    if (input.value.length < 6) {
        input.setCustomValidity("At least 6 characters");
    }
   else {
       input.setCustomValidity("");
    }
}

//секундомер
startday = new Date();
clockStart = startday.getTime();

function initStopwatch() {
    var myTime = new Date();
    var timeNow = myTime.getTime();
    var timeDiff = timeNow - clockStart;
    this.diffSecs = timeDiff / 1000;
    return (this.diffSecs);
}

function getSecs() {
    var mySecs = initStopwatch();
    var mySecs1 = "" + mySecs;
    mySecs1 = mySecs1.substring(0, mySecs1.indexOf(".")) + " secs.";
    document.forms[2].timespent.value = mySecs1
    window.setTimeout('getSecs()', 1000);
}

